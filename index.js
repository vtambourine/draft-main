var ACTIVE_CLASS = 'active';

$('.section').each(function(i) {
    var position = $(this).position();
    console.log(position);
    console.log('min: ' + position.top + ' / max: ' + parseInt(position.top + $(this).height()));
    var offset = $(this).height() / 2;
    $(this).scrollspy({
    	min: position.top - offset,
    	max: position.top + offset,

    	onEnter: function(element, position) {
    		if(console) console.log('entering ' +  element.id);
            console.log(element);
    		$(element).addClass(ACTIVE_CLASS);
    	},
    	onLeave: function(element, position) {
    		if(console) console.log('leaving ' +  element.id);
            $(element).removeClass(ACTIVE_CLASS);
    	}
    });
});
